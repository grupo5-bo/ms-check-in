﻿using ShareKernel.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckIn.Domain.Model.CheckIn
{
    public class CheckIn : AggregateRoot<Guid>
    {
        public Guid CheckInId { get; private set; }
        public DateTime HoraCheckIn { get; private set; }
        public Guid AdministrativoId { get; private set; }
        public Guid AsientoId { get; private set; }
        public ICollection<Equipaje> Equipajes { get; private set; }

        public CheckIn(Guid pCheckInId,DateTime pHoraCheckIn,Guid pAdministrativoId, Guid pAsientoId)
        {
            Id = Guid.NewGuid();
            HoraCheckIn = pHoraCheckIn;
            AdministrativoId = pAdministrativoId;
            AsientoId = pAsientoId;
            Equipajes = new List<Equipaje>();
        }

        public void AgregarEquipaje(Guid productoId)
        {
            //var detallePedido = Detalle.FirstOrDefault(x => x.ProductoId == productoId);
            //if (detallePedido is null)
            //{
            //    detallePedido = new DetallePedido(productoId, instrucciones, cantidad, precio);
            //    Detalle.Add(detallePedido);
            //}
            //else
            //{
            //    detallePedido.ModificarPedido(cantidad, precio);
            //}

            //Total = Total + detallePedido.SubTotal;

            //AddDomainEvent(new ItemPedidoAgregado(productoId, precio, cantidad));
        }

    }
}
