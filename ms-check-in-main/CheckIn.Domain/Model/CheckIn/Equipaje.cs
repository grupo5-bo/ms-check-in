﻿using CheckIn.Domain.Model.CheckIn.ValueObjects;
using ShareKernel.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckIn.Domain.Model.CheckIn
{
    public class Equipaje : Entity<Guid>
    {

        //TODO: Crear value objects para las propiedades
        public Guid EquipajeId { get; private set; }
        public string Descripcion { get; private set; }
        public Peso Peso { get; private set; }
        public bool EsFragil { get; private set; }
        public bool EsPeligroso { get; private set; }

        internal Equipaje(Guid pEquipajeId, string pDescripcion,
            Peso pPeso,
            bool pEsFragil, bool pEsPeligroso)
        {
            Id = Guid.NewGuid();
            EquipajeId = pEquipajeId;
            Descripcion = pDescripcion;
            Peso = pPeso;
            EsFragil = pEsFragil;
            EsPeligroso = pEsPeligroso;
        }

        //internal void ModificarPedido(int cantidad, decimal precio)
        //{
        //    Cantidad = cantidad;
        //    Precio = precio;
        //    SubTotal = precio * cantidad;
        //}

    }
}
