﻿using ShareKernel.Core;
using ShareKernel.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckIn.Domain.Model.CheckIn.ValueObjects
{
    public record Dimensiones : ValueObject
    {
        public double Alto { get; }
        public double Ancho { get; }
        public double Fondo { get; }
        public Unidad UnidadMedida { get; }
        public enum Unidad
        {
            CM, IN
        }

        public Dimensiones(double pAlto, double pAncho, double pFondo, Unidad pUnidadMedida)
        {
            CheckRule(new DoubleNotNullOrNegaviteRule(pAlto));
            CheckRule(new DoubleNotNullOrNegaviteRule(pAncho));
            CheckRule(new DoubleNotNullOrNegaviteRule(pFondo));
            Alto = pAlto;
            Ancho = pAncho;
            Fondo = pFondo;
            UnidadMedida = pUnidadMedida;
        }

        //public static implicit operator Dimensiones(Dimensiones value)
        //{
        //    return value;
        //}

        //public static implicit operator Dimensiones(Peso value)
        //{
        //    return new Dimensiones(value);
        //}
    }
}
