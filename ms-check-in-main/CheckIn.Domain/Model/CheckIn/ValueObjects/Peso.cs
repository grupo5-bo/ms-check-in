﻿using ShareKernel.Core;
using ShareKernel.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckIn.Domain.Model.CheckIn.ValueObjects
{
    public record Peso : ValueObject
    {
        public double Value { get; }

        public Peso(double value)
        {
            CheckRule(new DoubleNotNullOrNegaviteRule(value));
            Value = value;
        }

        public static implicit operator double(Peso value)
        {
            return value;
        }

        public static implicit operator Peso(double value)
        {
            return new Peso(value);
        }
    }
}
