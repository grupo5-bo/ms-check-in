﻿using ShareKernel.Core;
using ShareKernel.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckIn.Domain.Model.CheckIn
{
    public class Asiento : Entity<Guid>
    {
        public Guid AsientoId { get; private set; }
        public int Fila { get; }
        public string Letra { get; }
        public bool EsDeAltaPrioridad { get; }
        public bool EstaLibre { get; }

        internal Asiento(Guid pAsientoId, int pFila, string pLetra, bool pEsDeAltaPrioridad, bool pEstaLibre)
        {
            Id = Guid.NewGuid();
            AsientoId = pAsientoId;
            Fila = pFila;
            Letra = pLetra;
            EsDeAltaPrioridad = pEsDeAltaPrioridad;
            EstaLibre = pEstaLibre;
        }

    }
}
